package com.fomin.model.dao;

import com.fomin.exceptions.NameNotFoundException;
import com.fomin.model.domain.Animal;

import java.util.List;

public interface IZooDao {
    List<Animal> getAllAnimals();

    List<Animal> getAllAnimalsByName(String name) throws NameNotFoundException;

    void addAnimal(Animal animal);
}
