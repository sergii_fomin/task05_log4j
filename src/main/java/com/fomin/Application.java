package com.fomin;

/**
 * @author Fomin Sergii
 */

import com.fomin.controller.IZooController;
import com.fomin.model.dao.IZooDao;
import com.fomin.view.Menu;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Application {

    private static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("Initializing beans");
        IZooDao IZooDao = new com.fomin.model.dao.impl.IZooDaoImpl();
        IZooController IZooController = new com.fomin.controller.impl.IZooControllerImpl(IZooDao);
        logger.info("Initializing menu");
        logger.fatal("Sms test");
        new Menu(IZooController);

        logger.error("Exception", new IOException("Hi"));

        logger.trace("This si a trace manage");
        logger.debug("This si a trace manage");
        logger.info("This si a trace manage");
        logger.warn("This si a trace manage");
        logger.error("This si a trace manage");
        logger.fatal("This si a trace manage");
    }
}
