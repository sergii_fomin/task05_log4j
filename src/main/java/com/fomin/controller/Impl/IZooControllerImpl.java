package com.fomin.controller.impl;

import com.fomin.controller.IZooController;
import com.fomin.exceptions.NameNotFoundException;
import com.fomin.model.dao.IZooDao;
import com.fomin.model.domain.Animal;
import com.fomin.service.IZooFileService;
import com.fomin.service.impl.IZooFileServiceImpl;

import java.util.Scanner;

public class IZooControllerImpl implements IZooController {
    private IZooDao animalDao;

    public IZooControllerImpl(IZooDao animalDao) {
        this.animalDao = animalDao;
    }

    @Override
    public void printAllAnimals() {
        animalDao
                .getAllAnimals()
                .forEach(System.out::println);
    }

    @Override
    public void printAllAnimalsByName() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter name - ");
        String name = scanner.next();

        try {
            animalDao.getAllAnimalsByName(name).forEach(System.out::println);
        } catch (NameNotFoundException e) {
            System.out.println("Such name not found");
        }
    }

    @Override
    public void addAnimal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter animal data:");

        System.out.print("Name - ");
        String name = scanner.next();

        System.out.print("Age - ");
        int age = scanner.nextInt();

        System.out.print("Sex - ");
        String sex = scanner.next();

        animalDao.addAnimal(new Animal(
                name,
                age,
                sex
        ));
    }

    @Override
    public void saveAnimalsToFile() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter file name - ");
        String filepath = scanner.next();
        try (IZooFileService animalFileService = new IZooFileServiceImpl()) {
            animalFileService.saveAnimalsToFile(filepath, animalDao.getAllAnimals());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
