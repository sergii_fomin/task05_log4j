package com.fomin.controller;

public interface IZooController {
    void printAllAnimals();

    void printAllAnimalsByName();

    void addAnimal();

    void saveAnimalsToFile();
}
