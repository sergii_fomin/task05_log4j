package com.fomin.exceptions;

public class NameNotFoundException extends Exception {
    public NameNotFoundException(String message) {
        super(message);
    }
}
