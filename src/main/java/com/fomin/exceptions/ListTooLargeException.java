package com.fomin.exceptions;

public class ListTooLargeException extends RuntimeException {
    public ListTooLargeException(String message) {
        super(message);
    }
}
