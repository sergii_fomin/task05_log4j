package com.fomin.service.impl;

import com.fomin.model.domain.Animal;
import com.fomin.service.IZooFileService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;

public class IZooFileServiceImpl implements IZooFileService {
    @Override
    public void saveAnimalsToFile(String filename, List<Animal> animals) {
        try(FileOutputStream f = new FileOutputStream(new File(filename));
            ObjectOutputStream o = new ObjectOutputStream(f)) {
            animals.forEach(u -> {
                try {
                    o.writeObject(u);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws Exception {
        System.out.println("End");
    }
}
