package com.fomin.sms;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC6b516162f223cd8bb7c64cfe38b0fac5";
    public static final String AUTH_TOKEN = "62d5eb40551d4dca343ac3d0dd01421f";
    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380680100923"),
                        new PhoneNumber("+16579004753"), str) .create();
    }

}